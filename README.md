# kXML

## About

If you want to build a direct connection to our production/workflow systems and place orders this way, we prefer if you use our kXML for transferring the job data.

In this repository you'll find a sample kXML file and also (later on) an XSD.

## Field description

While most of the fields will probably be self-explanatory, we'll provide a short description to help you with identifying and matching the required fields in your systems.

| Field | Mandatory? | Description |
| ----- | ----- | ----------- |
| JobNo | Yes | This field contains your job number. This will be used for communicating with our support, it will also be printed on the packages to identify orders. Maximum length is 20 characters, ASCII only |
| UUID | - | If, for some reason, your systems are not able to identify or work with reponses containing only the _JobNo_, you can pass a _UUID_(-like) identifier for additional information. |
| Platform | Yes | Static value, will be provided by us to further identify your orders in our systems |
| CustomerNo | Yes | This is used for payment processing in SAP. If you want to have several invoices, one for each country or sth. like that, you'll need to change this value. Otherwise this is a static field, too. |
| CustomerReference | - | The customer reference will be added on the invoice but is otherwise not visible. This could be used to passthrough an external reference, like the project name of your customer. |
| Distributor | Yes | Static value, will be provided by us. This defines which set of mandatory information (IVB) is printed on the package. |
| ResponseMail | - | Response mail for automated preflight and other status reports |
| - | - | - |
| Article | Yes | Node for article related information. |
| ArtDescLang | Yes | Defines which language to use for mandatory information on package, should be the official language of the shipping country. Possible values are listed further down below on this page. Needs to be formatted in ISO 3166 ALPHA-2 coding ("DE", "AT", "CH"). |
| ArticleNo | Yes | The article number of this product. |
| Amount | Yes | Amount to be produced. |
| - | - | - |
| Shipping | Yes | Node for shipping related information. |
| ShipmentProvider | - | Shipment provider for this order. Will (until further notice) only be DHL. |
| ShippingType | - | Shipping type for this order. For the moment this is also limited to "DHL Paket" (for shipping inside of Germany) or "DHL Europaket" (for shipping in Europe). |
| ShippingLabel | - | If you want us to use your shipping label for this order, you can provide an URL to a label file here. Please note that our interfaces will not work in "mixed mode". You'll either always have to supply a label or never. |
| Receiver | Yes | This node holds information about the receiver of this order. |
| Company | - | Company name |
| FirstName | - | First name of the receiver |
| LastName | Yes | Last name of the receiver. At least this information must be given to successfully create label with DHL. |
| Addendum | - | This might contain information like the "Packstationsnummer" or sth. like "Marketing Department" or "4th Floor". Do not use name fields for information like that. |
| Street | Yes | Full street adress. |
| Zip | Yes | Zip code of the city |
| City | Yes | Name of the city |
| Country | Yes | Country code of the receiver country in ISO 3166 ALPHA-2 coding ("DE", "AT", "CH") |
| Phone | - | Phone number of the receiver. Some shipment providers might use mobile numbers to create push notifications. |
| Email | - | Email address of the receiver. Same as before, some providers might use this information to send a mail and inform about an upcoming shipment. KSW systems will NOT send mails to this address with tracking information or sth. alike. Those information will only be sent to the "ResponseMail" from above. |
| Sender | - | This node holds information about the receiver of this order. Same fields as for the Receiver are possible. These will be passed to DHL, so you can show up on the label. If you provide a label file, these will be ignored. Also note that the Sender address must be located in Germany,  |

## Minimum working example

The following is a minimum working example for placing orders with us through our interfaces. Please note that Platform and CustomerNo will have to be defined before you start placing orders. Contact us beforehand!

```xml
<?xml version="1.0" encoding="utf-8" ?>
<kxml xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.4">
    <Order>
        <JobNo>MyFirstTestOrder</JobNo>
        <Platform>???</Platform>
        <CustomerNo>???</CustomerNo>
        <Distributor>13</Distributor>
        <Article>
            <ArtDescLang>DE</ArtDescLang>
            <ArticleNo>110411101</ArticleNo>
            <Amount>500</Amount>
        </Article>
        <Shipping>
            <Receiver>
                <LastName>Meier</LastName>
                <Street>Holzmattenstraße 22</Street>
                <Zip>79336</Zip>
                <City>Herbolzheim</City>
                <Country>DE</Country>
            </Receiver>
        </Shipping>
    </Order>    
</kxml>
```

(For a full file structure, see the XML file in this repository.)

## Allowed countries

Food regulation laws (the "Lebensmittelinformationsverordnung") requires us to print mandatory information like nutrients, allergenes, best before date and more on the packages in (one of the) official language(s) in the country. We can provide this information for a quite a few European countries already, these are the countries we can ship your orders to.

Country codes have to be provided with their ISO 3166 ALPHA-2 codes as seen in the table down below.

| Code | Language of the overlay texts |
| ----- | ----------- |
| DE | German |
| AT | German |
| CH | German |
| DK | Danish |
| ES | Spanish |
| FR | French |
| BE | French |
| GB | English |
| IE | English |
| IT | Italian |
| NL | Dutch |
| SE | Swedish |
| LU | French |
| EE | Estonian |
| FI | Finish |
| HU | Hungarian |
| PL | Polish |

Please do not try to trick this system by sending false or misleading information because you want a different language on the packages. This might lead to legal consequences for you or your customers.

## Contact
For any questions, please contact us via support@suesse-werbung.de only. 

Mails to this address will land in our Freshdesk, where we have lots of eyes on them quickly and can distribute it across the team(s) in charge.